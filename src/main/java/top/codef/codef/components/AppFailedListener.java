//package top.codef.codef.components;
//
//import java.util.List;
//
//import org.springframework.boot.context.event.ApplicationFailedEvent;
//import org.springframework.cloud.client.serviceregistry.Registration;
//import org.springframework.context.ApplicationListener;
//
//import top.codef.codef.entities.ProjectState;
//import top.codef.codef.entities.enums.ServerState;
//import top.codef.codef.feign.ServiceStateNotice;
//import top.codef.codef.properties.PrometheusCodefCloudProperties;
//
//public class AppFailedListener implements ApplicationListener<ApplicationFailedEvent> {
//
//	private final ServicePreRegistListener servicePreRegistListener;
//
//	private final PrometheusCodefCloudProperties prometheusCodefCloudProperties;
//
//	private final List<ServiceStateNotice> serviceStateNotices;
//
//	/**
//	 * @param servicePreRegistListener
//	 * @param prometheusCodefCloudProperties
//	 */
//	public AppFailedListener(ServicePreRegistListener servicePreRegistListener,
//			PrometheusCodefCloudProperties prometheusCodefCloudProperties,
//			List<ServiceStateNotice> serviceStateNotices) {
//		this.servicePreRegistListener = servicePreRegistListener;
//		this.prometheusCodefCloudProperties = prometheusCodefCloudProperties;
//		this.serviceStateNotices = serviceStateNotices;
//	}
//
//	@Override
//	public void onApplicationEvent(ApplicationFailedEvent event) {
//		String appName = event.getApplicationContext().getEnvironment().getProperty("spring.application.name");
//		Registration registration = servicePreRegistListener.getRegistration();
//		ProjectState projectState = new ProjectState(appName, registration.getServiceId(), registration.getInstanceId(),
//				prometheusCodefCloudProperties.getTenantId(), ServerState.OUT_OF_SERVICE,
//				event.getException().getMessage());
//		serviceStateNotices.forEach(x -> x.notice(projectState));
//	}
//
//}
