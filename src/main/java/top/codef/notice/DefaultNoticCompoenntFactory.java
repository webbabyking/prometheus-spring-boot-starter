package top.codef.notice;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import top.codef.config.notice.NoticeSendComponentRegister;

public class DefaultNoticCompoenntFactory implements NoticeComponentFactory {

	private final Map<String, List<INoticeSendComponent>> map = new ConcurrentHashMap<>();

	public DefaultNoticCompoenntFactory(NoticeSendComponentRegister componentRegister) {
		this.map.putAll(componentRegister.getRegistComponent());
	}

	@Override
	public List<INoticeSendComponent> get(String blameFor) {
		return map.get(blameFor);
	}

}
