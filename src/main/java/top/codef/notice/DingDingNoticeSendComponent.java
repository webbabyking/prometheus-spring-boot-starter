package top.codef.notice;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import top.codef.httpclient.DingdingHttpClient;
import top.codef.pojos.PromethuesNotice;
import top.codef.pojos.dingding.DingDingNotice;
import top.codef.pojos.dingding.DingDingResult;
import top.codef.properties.enums.DingdingTextType;
import top.codef.properties.notice.DingDingNoticeProperty;
import top.codef.text.NoticeTextResolver;

public class DingDingNoticeSendComponent implements INoticeSendComponent {

	private final DingdingHttpClient httpClient;

	private final DingDingNoticeProperty dingDingNoticeProperty;

	private final Log logger = LogFactory.getLog(DingDingNoticeSendComponent.class);

	private final DingdingTextType dingdingTextType;

	private static final String NAME = "dingding";

	public DingDingNoticeSendComponent(DingdingHttpClient httpClient, DingDingNoticeProperty dingDingNoticeProperty,
			DingdingTextType dingdingTextType) {
		this.httpClient = httpClient;
		this.dingDingNoticeProperty = dingDingNoticeProperty;
		this.dingdingTextType = dingdingTextType;
	}

	/**
	 * @return the httpClient
	 */
	public DingdingHttpClient getHttpClient() {
		return httpClient;
	}

	@Override
	public void send(PromethuesNotice exceptionNotice, NoticeTextResolver noticeTextResolver) {
		String noticeText = noticeTextResolver.resolve(exceptionNotice);
		DingDingNotice dingDingNotice = dingDingNoticeProperty.generateDingdingNotice(noticeText,
				exceptionNotice.getTitle(), dingdingTextType);
		DingDingResult result = httpClient.doSend(dingDingNotice, dingDingNoticeProperty);
		if (logger.isTraceEnabled())
			logger.trace(result);
	}

	@Override
	public String name() {
		return NAME;
	}

}
