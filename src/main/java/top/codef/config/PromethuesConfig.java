package top.codef.config;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import top.codef.config.conditions.PrometheusEnabledCondition;
import top.codef.config.notice.NoticeSendComponentRegister;
import top.codef.config.notice.NoticeTextResolverCustomer;
import top.codef.exceptionhandle.components.InMemeryExceptionStatisticsRepository;
import top.codef.exceptionhandle.interfaces.NoticeStatisticsRepository;
import top.codef.httpclient.DefaultDingdingHttpClient;
import top.codef.httpclient.DingdingHttpClient;
import top.codef.properties.PrometheusProperties;
import top.codef.text.NoticeTextResolver;
import top.codef.text.NoticeTextResolverProvider;

@Configuration
@Conditional(PrometheusEnabledCondition.class)
@EnableConfigurationProperties(PrometheusProperties.class)
public class PromethuesConfig {

	@Bean
	public NoticeSendComponentRegister noticeSendComponentRegister() {
		return new NoticeSendComponentRegister();
	}

	@Bean
	public NoticeTextResolverProvider noticeTextResolverProvider(List<NoticeTextResolver> list,
			List<NoticeTextResolverCustomer> resolverCustomers) {
		NoticeTextResolverProvider provider = new NoticeTextResolverProvider(list);
		resolverCustomers.forEach(x -> x.custom(provider));
		return provider;
	}

	@Bean
	@ConditionalOnMissingBean
	public DingdingHttpClient dingdingHttpClient(ObjectMapper objectMapper) {
		DingdingHttpClient dingdingHttpClient = new DefaultDingdingHttpClient(objectMapper);
		return dingdingHttpClient;
	}

	@Bean
	@ConditionalOnMissingBean
	public NoticeStatisticsRepository exceptionNoticeStatisticsRepository() {
		NoticeStatisticsRepository repository = new InMemeryExceptionStatisticsRepository();
		return repository;
	}
}
