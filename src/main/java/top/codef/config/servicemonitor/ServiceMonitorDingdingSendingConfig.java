package top.codef.config.servicemonitor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import top.codef.config.annos.ConditionalOnServiceMonitor;
import top.codef.config.notice.NoticeTextResolverCustomer;
import top.codef.properties.enums.DingdingTextType;
import top.codef.properties.notice.PrometheusNoticeProperties;
import top.codef.text.NoticeTextResolverProvider;
import top.codef.text.markdown.ServiceMonitorMarkdownResolver;

@Configuration
@ConditionalOnServiceMonitor
public class ServiceMonitorDingdingSendingConfig implements NoticeTextResolverCustomer {

	private final Log logger = LogFactory.getLog(ServiceMonitorDingdingSendingConfig.class);

	@Autowired
	private PrometheusNoticeProperties prometheusNoticeProperties;

	@Override
	public void custom(NoticeTextResolverProvider resolverProvider) {
		logger.debug("创建钉钉markdown解析——服务异常通知");
		if (prometheusNoticeProperties.getDingdingTextType() == DingdingTextType.MARKDOWN)
		resolverProvider.add(new ServiceMonitorMarkdownResolver());
	}

	@Override
	public int getOrder() {
		return Ordered.LOWEST_PRECEDENCE;
	}

}
