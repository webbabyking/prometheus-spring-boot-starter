package top.codef.config.notice;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import top.codef.config.conditions.PrometheusEnabledCondition;
import top.codef.notice.DefaultNoticCompoenntFactory;
import top.codef.notice.NoticeComponentFactory;
import top.codef.properties.notice.PrometheusNoticeProperties;

@Configuration
@EnableConfigurationProperties({ PrometheusNoticeProperties.class })
@Conditional(PrometheusEnabledCondition.class)
public class NoticeAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean
	public NoticeComponentFactory noticeComponentFactory(List<NoticeSendComponentCustomer> customers,
			NoticeSendComponentRegister noticeSendComponentRegister) {
		customers.forEach(customer -> customer.custom(noticeSendComponentRegister));
		DefaultNoticCompoenntFactory compoenntFactory = new DefaultNoticCompoenntFactory(noticeSendComponentRegister);
		return compoenntFactory;
	}
}
