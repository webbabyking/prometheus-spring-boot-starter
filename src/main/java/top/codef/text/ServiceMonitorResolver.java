package top.codef.text;

import top.codef.exceptions.PrometheusException;
import top.codef.pojos.PromethuesNotice;
import top.codef.pojos.servicemonitor.ServiceCheckNotice;

public interface ServiceMonitorResolver extends NoticeTextResolver {

	default String resolve(PromethuesNotice notice) {
		if (notice instanceof ServiceCheckNotice) {
			ServiceCheckNotice serviceCheckNotice = (ServiceCheckNotice) notice;
			return serviceMonitorResolve(serviceCheckNotice);
		} else
			throw new PrometheusException("the type of notice is incorrect !");

	}

	String serviceMonitorResolve(ServiceCheckNotice serviceCheckNotice);
}
